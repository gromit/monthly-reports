---
title: "December"
date: 2022-01-09T22:41:12+01:00
---

# Arch Linux in December 2021

## Staff

We are happy to welcome [Segaja][0] among the Trusted Users.

## Monthly Report

We have switched the format of the monthly report to specifically report the
progress of the whole previous month.

## Debug packages

The dbscripts repository handling for debug packages has successfully been
[merged][1] and deployed. Successive integration has been [submitted to
devtools][2] as well as pacman improvements related to [PKGINFO][3] and
[debugedit][4].

## Keyring

As the last step of migrating our keyring to a new tooling and a local source
of truth we have re-boostrapped the trust with an [initial import][5] of the
old trust data into the new curated data structure [6]. We are still inviting
people to look at the current trust data and cross verify the results.

## pacman

A new library dependency feature has been [submitted to makepkg][6].  At the
end of package building, makepkg will look in the library directories and auto
add a library provide that packages can auto depend on.

[Patches were submitted][7] to alternatively choose asignify for package
signatures while keeping gnupg as default in place.

## LTO

With the availability of the latest devtools release a lot of freshly updated
packages were built with LTO. We have encountered different set of issues that
needed to be tackled. Some language specific problems have popped up including
but not limited to Go and Rust that require some documentation and adjustments.

## ArchWiki

After being broken for almost four months, ArchWiki is once again readable on
small screens.

[0]: https://www.mail-archive.com/aur-general@lists.archlinux.org/msg00635.html
[1]: https://gitlab.archlinux.org/archlinux/dbscripts/-/merge_requests/21
[2]: https://github.com/archlinux/devtools/pull/78
[3]: https://lists.archlinux.org/pipermail/pacman-dev/2022-January/025464.html
[4]: https://lists.archlinux.org/pipermail/pacman-dev/2022-January/025471.html
[5]: https://gitlab.archlinux.org/archlinux/archlinux-keyring
[6]: https://lists.archlinux.org/pipermail/arch-dev-public/2021-December/030582.html
[7]: https://lists.archlinux.org/pipermail/pacman-dev/2022-January/025439.html
