---
title: "February"
date: 2023-03-18T14:45:37+01:00
---

# Arch Linux in February 2023

## Git packaging sources

All major workflow and usability requirements for the `pkgctl` tooling have
been finished and a experimental `devtools-git-poc` package has been put into
the repositories. Furthermore the proof of concept sandbox environment has been
set up and rolled out to anyone interested in testing. In the current phase we
will [collect feedback][0] to catch bugs and further usability improvements. We
are still very eagerly seeking for more testers.

## what can I do for Arch Linux

Our new contributor website [https://whatcanidofor.archlinux.org][1] has been
[announced][2]. We would like to express gratitude towards the current, past as
well as initial contributors for getting the ball on the road. There have been
some changes since the announcement, including the Arch navbar more or less
being implemented and a decent amount of work in general.

## FOSDEM 2023

A lot of arch staff has visited FOSDEM 2023 in Brussels, where we exchanged
ideas, hacked on projects and participated in the open-source conference
shenanigans. Overall it was a great event that yet again improved our team
spirit.

Foxboron held a [talk at FOSDEM][3], he showcased how Arch Linux implemented
support for debug packages in 2022, the improvement that was made to the pacman
package manager, the infrastructure changes that were needed and how debug
packages are distributed through debuginfod.

[0]: https://lists.archlinux.org/archives/list/arch-dev-public@lists.archlinux.org/thread/GD55TGGZVAUNEULOVH5UGD6WE6GFWUJN/
[1]: https://whatcanidofor.archlinux.org/
[2]: https://lists.archlinux.org/archives/list/arch-dev-public@lists.archlinux.org/thread/UQ235IKFHYIFDDG3VHVNLUQBKZWXVACW/
[3]: https://fosdem.org/2023/schedule/event/debug_packages/
