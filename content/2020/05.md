---
title: May
date: 2020-06-03T19:05:50+02:00
---

# Arch Linux in May 2020

## Staff

### TU addition

We are happy to [welcome Frederik aka freswa][0] among the Trusted Users. Some
of you may already know him as one of our bug wranglers where he joined forces
in February 2020.

## Packaging

### Go

The [Go package guidelines][1] have been overhauled and in conjunction the
go-pie package has been [removed][2]. The major difference is a new set of
CGO/GOFLAGS that ensure all our distro flags are respected appropriately,
leading to Go binaries with RELRO, PIE and fortify hardening. A to-do list to
reflect these changes is pending.

### CMake

The [CMake package guidelines][3] have been created which describe some
important bits to consider when packaging software using cmake. Most notably
appropriate release type option that may otherwise have undesired effects,
removal of non required RPATH usage as well as some convenience options to
build in subdirectories without manually creating them. As CMake still does not
respect CPPFLAGS itself, which results in fortify hardening being ignored, a
[temporary patch][4] has been added to circumvent this misbehavior, a to-do
list to reflect these changes is pending.

## Reproducible builds

You may have noticed a couple of large rebuilds that occurred recently. These
fixed issues of non-reproducible file ordering with old versions of makepkg.
This and other hard work by the team improving our tooling and fixing packaging
issues has resulted in 96% of [core] being reproducible, and 90% of [extra].
You can see the [status of which packages are reproducible][5]. A full progress
report can be found on the [corresponding thread][6] on `arch-dev-public`.

We have set up a fleet of three [rebuilderd][7] runners to continuously test our
distributed repository packages and populate our [status page][8]. Some
integration into archweb to indicate the current reproducibility status is
planned.

## Infrastructure

### GitLab

We're in the process of switching our Git hosting from our custom cgit instance
to GitLab! We created [gitlab.archlinux.org][9] and have started moving some
projects. We'll continue moving projects onto GitLab and will then get rid of
our cgit instance.

Users can currently not collaborate with us on GitLab as we still need to get
some monitoring in place to make sure we can keep a close eye on usages to ward
off abuse. However, we're planning on doing this soon and then we'll open up
GitLab to everyone. Finally you can collaborate on Arch like it's 2020!

We're also going to use GitLab for other things such as bug tracker (instead of
Flyspray), Kanban board (instead of Kanboard), and service desk (for GDPR
requests and such).

### Single-sign-on/Keycloak

Arch operates many different services - all of which with their own login
systems and account databases. This is not ideal from a security and
convenience standpoint. We'd like to enforce the same security requirements for
all users while also allowing everyone to use the same account to log in to all
of our services. It'll also finally allow you to use 2-factor authentication
for all Arch services.

We still have a long way to go here in terms of integrating all services via
SAML/OIDC and figuring out how to let users continue using their old accounts.

### SVN to Git migration

The git migration plans have been picked up again, and started working towards
a [proof-of-concept implementation][10]. This would allow packagers to avoid the
SVN mono repository and manage each package as a separate git repository, and
facilitate some modernization of our current tooling.  More information about
the plans and implementation will hit the `[arch-dev-public]` list in the
upcoming week.

## Projects

### Archiso

The archiso project has been [moved][11] to Arch Linux' [GitLab instance][12].
Furthermore over the past weeks smaller and larger fixes found their way into
the repository. In the future all merge requests and releases will be handled
via GitLab. CI integration and more elaborate test suites are still being
developed to ensure a more robust setup for our monthly release images and any
custom use cases.

### Pacman

Initial support for [parallel downloads][12] landed in the pacman code base,
requiring large changes to the codebase. Many patches providing the finishing
to this feature have been submitted. Once the code churn around this feature
request has slowed, we will make a beta release for wider testing.
Additionally, one obscure area of non-reproducility in packages was discovered
through the Arch Linux reproducible builds effort, and subsequently fixed in
makepkg. Furthermore, the move from using autotools to meson for the pacman
build system was completed. Discussion is ongoing on moving the pacman codebase
to the Arch Linux GitLab instance, with initial CI setup being added to the
codebase.

## SPI

For the upcoming SPI annual report 2019, a section about some of Arch Linux
achievements has been assembled to represent our project. A link to the report
will be distributed once it's available.

[0]: https://lists.archlinux.org/pipermail/aur-general/2020-May/035734.html
[1]: https://wiki.archlinux.org/index.php/Go_package_guidelines
[2]: https://lists.archlinux.org/pipermail/arch-dev-public/2020-March/029898.html
[3]: https://wiki.archlinux.org/index.php/CMake_package_guidelines
[4]: https://git.archlinux.org/svntogit/packages.git/commit/trunk/cmake-cppflags.patch?h=packages/cmake&id=bb5432e50cec3f3f7dff1fcbaa3446516b3cd23a
[5]: https://reproducible.archlinux.org/
[6]: https://lists.archlinux.org/pipermail/arch-dev-public/2020-May/029981.html
[7]: https://github.com/kpcyrd/rebuilderd
[8]: https://reproducible.archlinux.org/
[9]: https://gitlab.archlinux.org
[10]: https://github.com/Foxboron/dbscripts/commits/morten/git-support
[11]: https://lists.archlinux.org/pipermail/arch-releng/2020-May/003963.html
[12]: https://gitlab.archlinux.org/archlinux/archiso
[13]: https://git.archlinux.org/pacman.git/commit/?id=6a331af27fe6dc7842725d067fd2fb4a1c60c139
